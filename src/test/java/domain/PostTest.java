package domain;

import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.EntityTransaction;
import javax.persistence.Persistence;

public class PostTest {
  private EntityManagerFactory emf = Persistence.createEntityManagerFactory("KwetterTestPU");
  private EntityManager em;
  private EntityTransaction tx;

  @Before
  public void setUp() {
    em = emf.createEntityManager();
    tx = em.getTransaction();
  }

  @After
  public void tearDown() {

  }

  @Test
  public void testNewPost() {
    Post post = new Post("Hello", "07-03-2018");
    tx.begin();
    em.persist(post);
    tx.commit();

    Post foundPost = (Post) em.createQuery(
      "SELECT p FROM Post p WHERE p.content LIKE :content")
      .setParameter("content", post.getContent()).getSingleResult();

    Assert.assertEquals(post, foundPost);
  }

  @Test
  public void testPostGettersSetters() {
    User user = new User("NewUser", "password");
    tx.begin();
    em.persist(user);
    tx.commit();

    Post post = new Post();
    post.setId(1l);
    post.setContent("NewPost");
    post.setDateTime("07-03-2018");

    Assert.assertEquals(1l, (long) post.getId());
    Assert.assertEquals("NewPost", post.getContent());
    Assert.assertEquals("07-03-2018", post.getDateTime());
  }
}
