package domain;

import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import javax.persistence.*;
import java.math.BigInteger;

public class UserTest {
  private EntityManagerFactory emf = Persistence.createEntityManagerFactory("KwetterTestPU");
  private EntityManager em;
  private EntityTransaction tx;

  @Before
  public void setUp() {
    em = emf.createEntityManager();
    tx = em.getTransaction();
  }

  @After
  public void tearDown() {

  }

  @Test
  public void testNewUser() {
    User user = new User("NewUser", "password");
    tx.begin();
    em.persist(user);
    tx.commit();

    User foundUser = (User) em
      .createQuery("SELECT u FROM User u WHERE u.username LIKE :username")
      .setParameter("username", user.getUsername()).getSingleResult();

    Assert.assertEquals(user, foundUser);
  }

  @Test
  public void testNewUserAlreadyExisting() {
    User user = new User("NewUser", "password");
    tx.begin();
    em.persist(user);
    tx.commit();

    long count = ((BigInteger) em.createNativeQuery("SELECT COUNT(id) FROM user").getSingleResult()).longValue();

    tx.begin();
    em.persist(user);
    tx.commit();

    long secondCount = ((BigInteger) em.createNativeQuery("SELECT COUNT(id) FROM user").getSingleResult()).longValue();

    Assert.assertEquals(count, secondCount);
  }

  @Test
  public void testUserGettersSetters() {
    User user = new User();
    user.setId(1L);
    user.setUsername("NewUser");
    user.setPassword("password");
    user.setWeb("http://test.com");
    user.setBio("Hello");

    Assert.assertEquals(1L, (long) user.getId());
    Assert.assertEquals("NewUser", user.getUsername());
    Assert.assertEquals("password", user.getPassword());
    Assert.assertEquals("http://test.com", user.getWeb());
    Assert.assertEquals("Hello", user.getBio());
    Assert.assertEquals("id: " + user.getId() + " username: " + user.getUsername() + " password: " + user.getPassword(), user.toString());
  }
}
