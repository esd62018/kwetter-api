package service;

import domain.Post;
import org.jboss.arquillian.container.test.api.Deployment;
import org.jboss.arquillian.junit.Arquillian;
import org.jboss.shrinkwrap.api.ShrinkWrap;
import org.jboss.shrinkwrap.api.asset.EmptyAsset;
import org.jboss.shrinkwrap.api.spec.JavaArchive;
import org.junit.Test;
import org.junit.runner.RunWith;

import javax.inject.Inject;

import static org.junit.Assert.*;

@RunWith(Arquillian.class)
public class PostServiceTest {
  @Deployment
  public static JavaArchive createDeployment() {
    return ShrinkWrap.create(JavaArchive.class)
      .addPackages(true, "dao", "domain", "service")
      .addAsResource("insert.sql", "META-INF/insert.sql")
      .addAsResource("test-persistence.xml", "META-INF/persistence.xml")
      .addAsManifestResource(EmptyAsset.INSTANCE, "beans.xml");
  }

  @Inject
  private PostService postService;

  @Test
  public void testFindPostsFromUser() {
    int expectedAmountOfPosts = 3;
    int actualAmountOfPosts = postService.findPostsFromUser(1L).size();

    assertEquals(expectedAmountOfPosts, actualAmountOfPosts);
  }

  @Test
  public void testFindPostsWhereContentLike() {
    int expectedAmountOfPosts = 3;
    int actualAmountOfPosts = postService.findPostsWhereContentLike("post").size();

    assertEquals(expectedAmountOfPosts, actualAmountOfPosts);
  }

  @Test
  public void testFindPostsWhereUserIsMentionedIn() {
    int expectedAmountOfPosts = 1;
    int actualAmountOfPosts = postService.findPostsWhereUserIsMentionedIn("john").size();

    assertEquals(expectedAmountOfPosts, actualAmountOfPosts);
  }

  @Test
  public void testCreate() {
    Post newPostInstance = new Post(1L, "new post", "2018-01-22");
    Post newPostPersisted = postService.create(newPostInstance);

    assertEquals(newPostInstance.getContent(), newPostPersisted.getContent());
    assertEquals(newPostInstance.getDateTime(), newPostPersisted.getDateTime());
  }

  @Test
  public void testUpdate() {
    Post postInstanceUpdated = new Post(1L,"post updated", "2018-01-22");
    Post postPersistedUpdated = postService.update(postInstanceUpdated);

    assertEquals(postInstanceUpdated.getContent(), postPersistedUpdated.getContent());
    assertEquals(postInstanceUpdated.getDateTime(), postPersistedUpdated.getDateTime());
  }

  @Test
  public void testDelete() {
    postService.delete(1L);

    int expectedAmountOfPosts = 3;
    int actualAmountOfPosts = postService.findPostsWhereContentLike("post").size();

    assertEquals(expectedAmountOfPosts, actualAmountOfPosts);
  }
}
