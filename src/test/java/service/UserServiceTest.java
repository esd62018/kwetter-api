package service;

import domain.User;
import org.jboss.arquillian.container.test.api.Deployment;
import org.jboss.arquillian.junit.Arquillian;
import org.jboss.shrinkwrap.api.ShrinkWrap;
import org.jboss.shrinkwrap.api.asset.EmptyAsset;
import org.jboss.shrinkwrap.api.spec.JavaArchive;
import org.junit.Test;
import org.junit.runner.RunWith;

import javax.inject.Inject;
import javax.mail.MessagingException;

import java.security.NoSuchAlgorithmException;

import static org.junit.Assert.*;

@RunWith(Arquillian.class)
public class UserServiceTest {
  @Deployment
  public static JavaArchive createDeployment() {
    return ShrinkWrap.create(JavaArchive.class)
      .addPackages(true, "dao", "domain", "service")
      .addAsResource("insert.sql", "META-INF/insert.sql")
      .addAsResource("test-persistence.xml", "META-INF/persistence.xml")
      .addAsManifestResource(EmptyAsset.INSTANCE, "beans.xml");
  }

  @Inject
  private UserService userService;

  @Test
  public void testGetUsers() {
    int expectedAmountOfUsers = 7;
    int actualAmountOfUsers = userService.getUsers().size();

    assertEquals(expectedAmountOfUsers, actualAmountOfUsers);
  }

  @Test
  public void testAuthenticate() throws NoSuchAlgorithmException {
    User jimInstance = new User(1L, "jim", "password");

    String expectedToString = jimInstance.toString();
    String actualToString = userService.authenticate(jimInstance).toString();

    assertEquals(expectedToString, actualToString);
  }

  @Test
  public void testSignUp() throws NoSuchAlgorithmException, MessagingException {
    User newUserInstance = new User("new user", "password");
    User newUserPersisted = userService.signUp(newUserInstance);

    assertEquals(newUserInstance.getUsername(), newUserPersisted.getUsername());
    assertEquals(newUserInstance.getPassword(), newUserPersisted.getPassword());
  }

  @Test
  public void testUpdate() {
    User jimInstanceUpdated = new User(1L,"jim updated", "password updated");
    User jimPersistedUpdated = userService.update(jimInstanceUpdated);

    assertEquals(jimInstanceUpdated.getUsername(), jimPersistedUpdated.getUsername());
    assertEquals(jimInstanceUpdated.getPassword(), jimPersistedUpdated.getPassword());
  }

  @Test
  public void testDelete() {
    userService.delete(7L);

    int newExpectedAmountOfUsers = 6;
    int newActualAmountOfUsers = userService.getUsers().size();

    assertEquals(newExpectedAmountOfUsers, newActualAmountOfUsers);
  }
}
