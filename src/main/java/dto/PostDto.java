package dto;

import domain.Post;
import utils.Constants;

import java.util.List;
import java.util.stream.Collectors;

public class PostDto {
  private static final String API_POSTS_URL = Constants.BASE_API_URL + "posts/";
  private static final String API_RATINGS_URL = API_POSTS_URL + "ratings/";

  private Long id;
  private String content;
  private String dateTime;
  private UserDto poster;
  private List<RatingDto> ratings;

  private String updateLink;
  private String deleteLink;
  private String postRatingLink;

  public PostDto(Long id, String content, String dateTime, UserDto poster, List<RatingDto> ratings) {
    this.id = id;
    this.content = content;
    this.dateTime = dateTime;
    this.poster = poster;
    this.ratings = ratings;

    this.constructLinks();
  }

  public Long getId() {
    return id;
  }

  public void setId(Long id) {
    this.id = id;
  }

  public String getContent() {
    return content;
  }

  public void setContent(String content) {
    this.content = content;
  }

  public String getDateTime() {
    return dateTime;
  }

  public void setDateTime(String dateTime) {
    this.dateTime = dateTime;
  }

  public UserDto getPoster() {
    return poster;
  }

  public void setPoster(UserDto poster) {
    this.poster = poster;
  }

  public List<RatingDto> getRatings() {
    return ratings;
  }

  public void setRatings(List<RatingDto> ratings) {
    this.ratings = ratings;
  }

  public String getUpdateLink() {
    return updateLink;
  }

  public String getDeleteLink() {
    return deleteLink;
  }

  public String getPostRatingLink() {
    return postRatingLink;
  }

  private void constructLinks() {
    updateLink = API_POSTS_URL;
    deleteLink = API_POSTS_URL + String.valueOf(id);
    postRatingLink = API_RATINGS_URL + String.valueOf(id);
  }

  public static PostDto fromPost(Post post) {
    return new PostDto(
      post.getId(),
      post.getContent(),
      post.getDateTime(),
      UserDto.fromUserWithoutFollowers(post.getPoster()),
      post.getRatings().stream().map(RatingDto::fromRating).collect(Collectors.toList())
    );
  }
}
