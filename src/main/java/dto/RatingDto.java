package dto;

import domain.Rating;
import utils.Constants;

public class RatingDto {
  private static final String API_RATINGS_URL = Constants.BASE_API_URL + "posts/ratings/";

  private Long id;
  private Long postId;
  private Long userId;

  private String deleteLink;

  public RatingDto() {
    this.constructLinks();
  }

  public RatingDto(Long postId, Long userId) {
    this.id = id;
    this.postId = postId;
    this.userId = userId;

    this.constructLinks();
  }

  public RatingDto(Long id, Long postId, Long userId) {
    this.id = id;
    this.postId = postId;
    this.userId = userId;

    this.constructLinks();
  }

  public Long getId() {
    return id;
  }

  public void setId(Long id) {
    this.id = id;
  }

  public Long getPostId() {
    return postId;
  }

  public void setPostId(Long postId) {
    this.postId = postId;
  }

  public Long getUserId() {
    return userId;
  }

  public void setUserId(Long userId) {
    this.userId = userId;
  }

  public String getDeleteLink() {
    return deleteLink;
  }

  private void constructLinks() {
    this.deleteLink = API_RATINGS_URL + String.valueOf(postId) + "/" + String.valueOf(id);
  }

  public static RatingDto fromRating(Rating rating) {
    return new RatingDto(
      rating.getId(),
      rating.getRatedPost().getId(),
      rating.getRater().getId()
    );
  }
}
