package dto;

import domain.User;
import utils.Constants;

import javax.ws.rs.core.Link;
import java.util.List;
import java.util.stream.Collectors;

public class UserDto {
  private static final String API_USER_URL = Constants.BASE_API_URL + "users/";
  private static final String API_POSTS_URL = Constants.BASE_API_URL + "posts/";

  private Long id;
  private String username;
  private String password;
  private String web;
  private String bio;
  private String imageUrl;
  private UserRoleDto role;
  private int isConfirmed;
  private List<FollowerDto> follows;
  private List<FollowerDto> followedBy;

  private String selfLink;
  private String updateLink;
  private String userPostsLink;
  private String timelinePostsLink;
  private String mentionedPostsLink;

  public UserDto(Long id, String username, String password, String web, String bio,
                 String imageUrl, UserRoleDto role, int isConfirmed) {
    this.id = id;
    this.username = username;
    this.password = password;
    this.web = web;
    this.bio = bio;
    this.imageUrl = imageUrl;
    this.role = role;
    this.isConfirmed = isConfirmed;

    this.constructLinks();
  }

  public UserDto(Long id, String username, String password, String web, String bio,
                 String imageUrl, UserRoleDto role, int isConfirmed, List<FollowerDto> follows, List<FollowerDto> followedBy) {
    this.id = id;
    this.username = username;
    this.password = password;
    this.web = web;
    this.bio = bio;
    this.imageUrl = imageUrl;
    this.role = role;
    this.isConfirmed = isConfirmed;
    this.follows = follows;
    this.followedBy = followedBy;

    this.constructLinks();
  }

  public Long getId() {
    return id;
  }

  public void setId(Long id) {
    this.id = id;
  }

  public String getUsername() {
    return username;
  }

  public void setUsername(String username) {
    this.username = username;
  }

  public String getPassword() {
    return password;
  }

  public void setPassword(String password) {
    this.password = password;
  }

  public String getWeb() {
    return web;
  }

  public void setWeb(String web) {
    this.web = web;
  }

  public String getBio() {
    return bio;
  }

  public void setBio(String bio) {
    this.bio = bio;
  }

  public String getImageUrl() {
    return imageUrl;
  }

  public void setImageUrl(String imageUrl) {
    this.imageUrl = imageUrl;
  }

  public UserRoleDto getRole() {
    return role;
  }

  public void setRole(UserRoleDto role) {
    this.role = role;
  }

  public int getIsConfirmed() {
    return isConfirmed;
  }

  public void setConfirmed(int confirmed) {
    isConfirmed = confirmed;
  }

  public List<FollowerDto> getFollows() {
    return follows;
  }

  public void setFollows(List<FollowerDto> follows) {
    this.follows = follows;
  }

  public List<FollowerDto> getFollowedBy() {
    return followedBy;
  }

  public void setFollowedBy(List<FollowerDto> followedBy) {
    this.followedBy = followedBy;
  }

  public String getProfileLink() {
    return selfLink;
  }

  public String getUpdateLink() {
    return updateLink;
  }

  public String getUserPostsLink() {
    return userPostsLink;
  }

  public String getTimelinePostsLink() {
    return timelinePostsLink;
  }

  public String getMentionedPostsLink() {
    return mentionedPostsLink;
  }

  private void constructLinks() {
    selfLink = API_USER_URL + username;
    updateLink = API_USER_URL;
    userPostsLink = API_POSTS_URL + String.valueOf(id);
    timelinePostsLink = API_POSTS_URL + "timeline/" + String.valueOf(id);
    mentionedPostsLink = API_POSTS_URL + "mentioned/" + String.valueOf(id);

//    setSelfLink(
//      Link.fromUri(API_USER_URL + getUsername())
//        .rel("selfLink")
//        .type("GET")
//        .build()
//    );
//
//    setUpdateLink(
//      Link.fromUri(API_USER_URL)
//        .rel("updateLink")
//        .type("PUT")
//        .build()
//    );
//
//    setUserPostsLink(
//      Link.fromUri(API_POSTS_URL + String.valueOf(getId()))
//        .rel("userPostsLink")
//        .type("GET")
//        .build()
//    );
//
//    setTimelinePostsLink(
//      Link.fromUri(API_POSTS_URL + "timeline/" + String.valueOf(getId()))
//        .rel("timelinePostsLink")
//        .type("GET")
//        .build()
//    );
//
//    setMentionedPostsLink(
//      Link.fromUri(API_POSTS_URL + "mentioned/" + String.valueOf(getId()))
//        .rel("mentionedPostsLink")
//        .type("GET")
//        .build()
//    );
  }

  public static UserDto fromUser(User user) {
    return new UserDto(
      user.getId(),
      user.getUsername(),
      user.getPassword(),
      user.getWeb(),
      user.getBio(),
      user.getImageUrl(),
      UserRoleDto.fromUserRole(user.getRole()),
      user.getIsConfirmed(),
      user.getFollows().stream().map(FollowerDto::fromUser).collect(Collectors.toList()),
      user.getFollowedBys().stream().map(FollowerDto::fromUser).collect(Collectors.toList())
    );
  }

  // This one will return a UserDto without followers and following
  public static UserDto fromUserWithoutFollowers(User user) {
    return new UserDto(
      user.getId(),
      user.getUsername(),
      user.getPassword(),
      user.getWeb(),
      user.getBio(),
      user.getImageUrl(),
      UserRoleDto.fromUserRole(user.getRole()),
      user.getIsConfirmed()
    );
  }
}
