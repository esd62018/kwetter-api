package dto;

import domain.UserRole;

public class UserRoleDto {
  private Long id;
  private String role;

  public UserRoleDto(Long id, String role) {
    this.id = id;
    this.role = role;
  }

  public Long getId() {
    return id;
  }

  public void setId(Long id) {
    this.id = id;
  }

  public String getRole() {
    return role;
  }

  public void setRole(String role) {
    this.role = role;
  }

  public static UserRoleDto fromUserRole(UserRole role) {
    return new UserRoleDto(
      role.getId(),
      role.getRole()
    );
  }
}
