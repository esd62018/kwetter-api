package controller;

import dto.PostDto;
import dto.UserDto;
import service.PostService;
import service.UserService;

import javax.enterprise.context.RequestScoped;
import javax.faces.context.FacesContext;
import javax.inject.Inject;
import javax.inject.Named;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.util.List;
import java.util.stream.Collectors;

@Named
@RequestScoped
public class AdminController {
  @Inject
  private UserService userService;

  @Inject
  private PostService postService;

  public AdminController() {

  }

  /**
   * Will logout a user by removing it from the session and redirect back to login
   *
   * @return    string to the page we want to redirect to (login)
   */
  public String logout() {
    FacesContext context = FacesContext.getCurrentInstance();
    HttpServletRequest request = (HttpServletRequest) context.getExternalContext().getRequest();
    HttpSession session = (HttpSession) context.getExternalContext().getSession(false);

    try {
      request.logout();
      session.invalidate();
    } catch (ServletException e) {
      e.printStackTrace();
    }

    return "login?faces-redirect=true";
  }

  /**
   * Will get all posts (ordered by date DESC)
   *
   * @see PostService#findAllPostsOrderedByDate()
   */
  public List<PostDto> getAllPosts() {
    return postService.findAllPostsOrderedByDate().stream().map(PostDto::fromPost).collect(Collectors.toList());
  }

  /**
   * @see PostService#delete(Long)
   */
  public void deletePost(Long id) {
    postService.delete(id);
  }

  /**
   * @see UserService#getUsers()
   */
  public List<UserDto> getUsers() {
    return userService.getUsers().stream().map(UserDto::fromUser).collect(Collectors.toList());
  }

  /**
   * @see UserService#delete(Long)
   */
  public void deleteUser(Long id) {
    userService.delete(id);
  }

  /**
   * @see UserService#switchUserRole(Long)
   */
  public void switchUserRole(Long id) {
    userService.switchUserRole(id);
  }
}
