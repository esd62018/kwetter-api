package controller;

import domain.User;
import service.UserService;

import javax.enterprise.context.SessionScoped;
import javax.faces.application.FacesMessage;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.inject.Inject;
import javax.inject.Named;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.io.Serializable;
import java.security.Principal;
import java.util.Map;

@Named
@SessionScoped
public class LoginController implements Serializable {
  private String username;
  private String password;

  @Inject
  private UserService userService;

  public LoginController() {

  }

  public String getUsername() {
    return username;
  }

  public void setUsername(String username) {
    this.username = username;
  }

  public String getPassword() {
    return password;
  }

  public void setPassword(String password) {
    this.password = password;
  }

  public String submitLogin() {
    FacesContext context = FacesContext.getCurrentInstance();
    ExternalContext externalContext = context.getExternalContext();
    HttpServletRequest request = (HttpServletRequest) externalContext.getRequest();

    try {
      request.login(username, password);
    } catch (ServletException e) {
      context.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Login failed!", null));

      return "login";
    }

    Principal principal = request.getUserPrincipal();
    User auth = userService.getUserByUsername(principal.getName());

    if (auth.getRole().getRole().equals("admin")) {
      externalContext.getSessionMap().put("user", auth);                    // <-- set auth in session
      return "admin?faces-redirect=true";                                   // <-- redirect admin
    } else {
      context.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "You\'ve no permissions!", null));

      return "login";
    }
  }
}
