package filter;

import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.MalformedJwtException;
import io.jsonwebtoken.SignatureException;

import javax.servlet.*;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebFilter(
  filterName = "AuthFilter",
  urlPatterns = { "/api/users/*", "/api/posts/*" })
public class AuthFilter implements Filter {


  @Override
  public void doFilter(ServletRequest req, ServletResponse res, FilterChain chain) throws ServletException, IOException {
    HttpServletRequest request = (HttpServletRequest) req;
    HttpServletResponse response = (HttpServletResponse) res;

    if (request.getRequestURI().contains("javax.faces.resource")) {
      chain.doFilter(request, response);

      return;
    }

    String token = request.getHeader("token");

    if (token == null) {
      response.sendError(403);

      return;
    }

    try {
      // OK, we can trust this JWT
      Jwts.parser().setSigningKey("SecretKey").parseClaimsJws(token);

      chain.doFilter(request, response);
    } catch (SignatureException | MalformedJwtException e) {
      // Don't trust the JWT!
      System.out.println("JWT e: " + e.toString());
      response.sendError(403);
    }
  }
}
