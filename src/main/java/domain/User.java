package domain;

import javax.persistence.*;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * User entity
 *
 * Simple. Has some properties which needs to be saved.
 *
 * Checkout the auth named query, this one will be used to login a user.
 *
 * All relations are handled through Post and Follower entities.
 */
@Entity
@Table(name = "User")
@NamedQueries({
  @NamedQuery(name = "User.findAll", query = "SELECT u FROM User u"),
  @NamedQuery(name = "User.findByUsername", query = "SELECT u FROM User u WHERE u.username = :username"),
  @NamedQuery(
    name = "User.auth",
    query = "SELECT u FROM User u WHERE u.username = :username AND u.password = :password"
  ),
  @NamedQuery(
    name = "User.setIsConfirmed",
    query = "UPDATE User SET isConfirmed = 1 WHERE username = :username AND password = :password"
  )
})
public class User implements Serializable {
  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  private Long id;

  @Column(unique = true, nullable = false)
  private String username;

  @Column(nullable = false)
  private String password;

  private String web;

  private String imageUrl;

  private String bio;

  @Column(name = "IS_CONFIRMED")
  private int isConfirmed;

  @ManyToMany(mappedBy = "follows", cascade = CascadeType.PERSIST)
  private List<User> followedBys = new ArrayList<>();

  @ManyToMany(cascade = CascadeType.PERSIST)
  private List<User> follows = new ArrayList<>();

  @OneToOne(mappedBy = "user", cascade = CascadeType.PERSIST)
  private UserRole role;

  @PrePersist
  public void prePersist() {
    if (this.role == null) {
      this.role = new UserRole("user", this);
    }

    isConfirmed = 0;
  }

  public User() {

  }

  public User(Long id) {
    this.id = id;
  }


  public User(String username, String password) {
    this.username = username;
    this.password = password;
  }

  public User(Long id, String username, String password) {
    this.id = id;
    this.username = username;
    this.password = password;
  }

  public Long getId() {
    return id;
  }

  public void setId(Long id) {
    this.id = id;
  }

  public String getUsername() {
    return username;
  }

  public void setUsername(String username) {
    this.username = username;
  }

  public String getPassword() {
    return password;
  }

  public void setPassword(String password) {
    this.password = password;
  }

  public String getWeb() {
    return web;
  }

  public void setWeb(String web) {
    this.web = web;
  }

  public String getImageUrl() {
    return imageUrl;
  }

  public void setImageUrl(String imageUrl) {
    this.imageUrl = imageUrl;
  }

  public String getBio() {
    return bio;
  }

  public void setBio(String bio) {
    this.bio = bio;
  }

  public List<User> getFollowedBys() {
    return followedBys;
  }

  public void setFollowedBys(List<User> followedBys) {
    this.followedBys = followedBys;
  }

  public void addFollowedBy(User followedBy) {
    followedBys.add(followedBy);
  }

  public void removeFollowedBy(User followedBy) {
    followedBys.remove(followedBy);
  }

  public List<User> getFollows() {
    return follows;
  }

  public void setFollows(List<User> follows) {
    this.follows = follows;
  }

  public void addFollow(User follow) {
    follows.add(follow);
  }

  public void removeFollow(User follow) {
    follows.remove(follow);
  }

  public UserRole getRole() {
    return role;
  }

  public void setRole(UserRole role) {
    this.role = role;
  }

  public int getIsConfirmed() {
    return isConfirmed;
  }

  public void setIsConfirmed(int isConfirmed) {
    this.isConfirmed = isConfirmed;
  }

  public void follow(User follow) {
    if (!follows.contains(follow)) {
      follows.add(follow);
      follow.addFollowedBy(this);
    }
  }

  public void unFollow(User follow) {
    if (follows.contains(follow)) {
      follows.remove(follow);
      follow.removeFollow(this);
    }
  }

  @Override
  public String toString() {
    return "id: " + id
      + " username: " + username
      + " password: " + password
      + " web: " + web
      + " bio: " + bio
      + " imageUrl: " + imageUrl;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) return true;
    if (!(o instanceof User)) return false;
    return id != null && id.equals(((User) o).id);
  }

  @Override
  public int hashCode() {
    return 31;
  }
}
