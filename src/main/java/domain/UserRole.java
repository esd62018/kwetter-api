package domain;

import javax.persistence.*;

import java.io.Serializable;
import java.util.List;

@Entity
@Table(name = "User_Role")
public class UserRole implements Serializable {
  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  private long id;

  private String role;

  public User getUser() {
    return user;
  }

  public void setUser(User user) {
    this.user = user;
  }

  @OneToOne
  @JoinColumn(name = "USERNAME", referencedColumnName = "USERNAME")
  private User user;  // ON DELETE CASCADE manually added in insert.sql script

  public UserRole() {

  }

  public UserRole(String role, User user) {
    this.role = role;
    this.user = user;
  }

  public long getId() {
    return id;
  }

  public void setId(long id) {
    this.id = id;
  }

  public String getRole() {
    return role;
  }

  public void setRole(String role) {
    this.role = role;
  }
}
