package domain;


import javax.persistence.*;

/**
 * Rating entity
 *
 * A post can have ratings, rated by a user.
 */
@Entity
@Table(name = "Rating")
public class Rating {
  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  private Long id;

  @ManyToOne
  @JoinColumn(name = "USER_ID")
  private User rater;

  @ManyToOne    // ON DELETE CASCADE manually added in insert.sql script
  @JoinColumn(name = "POST_ID")
  private Post ratedPost;

  public Rating() {

  }

  public Long getId() {
    return id;
  }

  public void setId(Long id) {
    this.id = id;
  }

  public User getRater() {
    return rater;
  }

  public void setRater(User rater) {
    this.rater = rater;
  }

  public Post getRatedPost() {
    return ratedPost;
  }

  public void setRatedPost(Post ratedPost) {
    this.ratedPost = ratedPost;
  }

  @Override
  public String toString() {
    return "id: " + id
      + " post id: " + ratedPost.getId()
      + " user id: " + rater.getId();
  }
}
