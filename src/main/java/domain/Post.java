package domain;

import javax.persistence.*;

import java.util.List;

/**
 * Post entity
 *
 * A post always gets posted by a user (see posterId)
 *
 * Note: using the named queries we won't be needing a @ManyToOne relation with the User entity :)
 */
@Entity
@Table(name = "Post")
@NamedQueries({
  @NamedQuery(
    name = "Post.findAllOrderedByDate",
    query = "SELECT p FROM Post p ORDER BY p.dateTime DESC"
  ),
  @NamedQuery(
    name = "Post.findAllFromUser",
    query = "SELECT p FROM Post p WHERE p.poster.id = :userId"
  ),
  @NamedQuery(
    name = "Post.findAllWhereContentLike",
    query = "SELECT p FROM Post p WHERE LOWER(p.content) LIKE LOWER(:content)"
  )
})
public class Post {
  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  private Long id;

  private String content;

  private String dateTime;

  @ManyToOne    // ON DELETE CASCADE manually added in insert.sql script
  private User poster;

  @OneToMany(mappedBy = "ratedPost", cascade = { CascadeType.PERSIST })
  private List<Rating> ratings;

  public Post() {

  }

  public Post(Long id) {
    this.id = id;
  }

  public Post(String content, String dateTime) {
    this.content = content;
    this.dateTime = dateTime;
  }

  public Post(Long id, String content, String dateTime) {
    this.id = id;
    this.content = content;
    this.dateTime = dateTime;
  }

  public Long getId() {
    return id;
  }

  public void setId(Long id) {
    this.id = id;
  }

  public String getContent() {
    return content;
  }

  public void setContent(String content) {
    this.content = content;
  }

  public String getDateTime() {
    return dateTime;
  }

  public void setDateTime(String dateTime) {
    this.dateTime = dateTime;
  }

  public User getPoster() {
    return poster;
  }

  public void setPoster(User poster) {
    this.poster = poster;
  }

  public List<Rating> getRatings() {
    return ratings;
  }

  public void setRatings(List<Rating> ratings) {
    this.ratings = ratings;
  }

  public void addRating(Rating rating) {
    ratings.add(rating);
  }

  public void deleteRating(Long id) {
    this.ratings.removeIf(rating -> rating.getId().equals(id));
  }

  @Override
  public String toString() {
    return "id: " + id
      + " content: " + content
      + " dateTime: " + dateTime;
  }
}
