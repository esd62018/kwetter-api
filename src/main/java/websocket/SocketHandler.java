package websocket;

import com.google.gson.Gson;
import com.google.gson.JsonObject;
import domain.Post;
import domain.User;
import dto.PostDto;
import service.PostService;
import service.UserService;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.websocket.Session;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.stream.Collectors;

@ApplicationScoped
public class SocketHandler {
  @Inject
  private PostService postService;

  @Inject
  private UserService userService;

  private final Map<Session, User> sessions = new HashMap<>();

  public SocketHandler() {
    // Empty
  }

  public void addSession(Session session) {
    Map<String, String> pathParams = session.getPathParameters();
    String username = pathParams.entrySet().iterator().next().getValue();

    try {
      User user = userService.getUserByUsername(username);

      sessions.put(session, user);
    } catch (Exception e) {
      e.printStackTrace();
    }
  }

  public void removeSession(Session session) {
    sessions.remove(session);
  }

  public void addPost(Session session, String json) {
    try {
      Post post = new Gson().fromJson(json, Post.class);
      PostDto postDto = PostDto.fromPost(postService.create(post));

      sendData(session, new Gson().toJson(postDto));

      sendPostToFollowers(postDto);
    } catch (Exception e) {
      System.out.println("Exception adding post: " + e.toString());
      e.printStackTrace();
    }
  }

  private void sendPostToFollowers(PostDto post) {
    String poster = post.getPoster().getUsername();

    for (Map.Entry<Session, User> entry: sessions.entrySet()) {
      Session session = entry.getKey();
      User user = entry.getValue();

      if (getUserIsFollowingPoster(user, poster)) {
        sendData(session, new Gson().toJson(post));
      }
    }
  }

  private boolean getUserIsFollowingPoster(User onlineUser, String poster) {
    return onlineUser
      .getFollows()
      .stream()
      .filter(u -> u.getUsername().equals(poster))
      .collect(Collectors.toList()).size() == 1;
  }

  private void sendData(Session session, String json) {
    try {
      session.getBasicRemote().sendText(json);
    } catch (IOException ex) {
      sessions.remove(session);
      Logger.getLogger(SocketHandler.class.getName()).log(Level.SEVERE, null, ex);
    }
  }
}
