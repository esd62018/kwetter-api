package websocket;

import com.google.gson.Gson;
import dto.PostDto;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.websocket.*;
import javax.websocket.server.ServerEndpoint;
import java.util.logging.Level;
import java.util.logging.Logger;

@ApplicationScoped
@ServerEndpoint("/actions")
public class SocketServer {

  @Inject
  private SocketHandler socketHandler;

  @OnOpen
  public void open(Session session) {
    socketHandler.addSession(session);
  }

  @OnClose
  public void close(Session session) {
    socketHandler.removeSession(session);
  }

  @OnError
  public void onError(Throwable error) {
    Logger.getLogger(SocketServer.class.getName()).log(Level.SEVERE, null, error);
  }

  @OnMessage
  public void handleMessage(String json, Session session) {
    socketHandler.addPost(session, json);
  }
}
