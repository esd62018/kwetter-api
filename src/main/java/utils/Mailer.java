package utils;

import java.util.Properties;

import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

public class Mailer {
  private static final String email = "email@gmail.com";
  private static final String password = "password";
  // TODO: change to actual email and password

  public static void sendConfirmationEmail(String recipient, String hash) throws MessagingException {
    Properties props = new Properties();
    props.put("mail.smtp.auth", "true");
    props.put("mail.smtp.starttls.enable", "true");
    props.put("mail.smtp.host", "smtp.gmail.com");
    props.put("mail.smtp.port", "587");

    Session session = Session.getInstance(props, new javax.mail.Authenticator() {
      protected PasswordAuthentication getPasswordAuthentication() {
        return new PasswordAuthentication(email, password);
      }
    });

    String link = Constants.BASE_API_URL + "mail/" + recipient + "/" + hash;
    String mailHtml = "Activate your account by clicking the following link: \n" + link;

    Message message = new MimeMessage(session);
    message.setFrom(new InternetAddress(email));
    message.setRecipients(Message.RecipientType.TO, InternetAddress.parse(recipient));
    message.setSubject("Activate your account now");
    message.setText(mailHtml);

    Transport.send(message);
  }
}
