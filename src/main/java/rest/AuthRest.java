package rest;

import domain.User;
import dto.UserDto;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import service.UserService;
import utils.Constants;

import javax.ejb.EJBTransactionRolledbackException;
import javax.inject.Inject;
import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.*;

import static javax.ws.rs.core.MediaType.APPLICATION_JSON;

@Path("/auth")
public class AuthRest {
  @Inject
  private UserService userService;

  public AuthRest() {

  }

  /**
   * Route to authenticated user with login credentials.
   *
   * Required request body JSON format:
   *  {
   *    "username": "username",
   *    "password": "password"
   *  }
   *
   * All above fields are required!
   *
   * @param user      user instance serialized from request body
   * @return          response containing an user in JSON and a JWT in JSON format
   */
  @POST
  @Path("/auth")
  @Consumes({ APPLICATION_JSON })
  @Produces({ APPLICATION_JSON })
  public Response authenticate(User user) {
    ResponseBody responseBody = new ResponseBody();
    int status = 200;

    try {
      UserDto userDto = UserDto.fromUser(userService.authenticate(user));

      if (userDto.getIsConfirmed() == 0) {
        return Response.status(406).build();
      }

      String token = Jwts.builder()
        .setSubject(String.valueOf(userDto.getId()))
        .signWith(SignatureAlgorithm.HS512, "SecretKey")
        .compact();

      responseBody.setAuth(userDto);
      responseBody.setToken(token);
    } catch (Exception e) {
      e.printStackTrace();

      if (e instanceof EJBTransactionRolledbackException) {   // Caused by no user found
        status = 404;
      } else {
        status = 500;
      }
    }

    return Response
      .status(status)
      .entity(responseBody)
      .build();
  }

  /**
   * Route to sign up a new user
   *
   * Required request body JSON format:
   *  {
   *    "username": "username",
   *    "password": "password"
   *  }
   *
   * All above fields are required!
   *
   * @param user      user instance serialized from request body
   * @return          response containing user instance in JSON format when successfully saved in the db
   */
  @POST
  @Path("/sign-up")
  @Consumes({ APPLICATION_JSON })
  @Produces({ APPLICATION_JSON })
  public Response signUp(User user) {
    UserDto userDto = null;
    int status = 200;

    try {
      userDto = UserDto.fromUser(userService.signUp(user));
    } catch (Exception e) {
      // TODO: explicit exception handling
      e.printStackTrace();

      status = 500;
    }

    return Response
      .status(status)
      .entity(userDto)
      .build();
  }

  public class ResponseBody {
    private UserDto auth;
    private String token;

    public ResponseBody() {

    }

    public UserDto getAuth() {
      return auth;
    }

    public void setAuth(UserDto auth) {
      this.auth = auth;
    }

    public String getToken() {
      return token;
    }

    public void setToken(String token) {
      this.token = token;
    }
  }
}
