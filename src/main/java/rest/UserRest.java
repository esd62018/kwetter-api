package rest;

import domain.User;
import dto.UserDto;
import service.UserService;

import static javax.ws.rs.core.MediaType.APPLICATION_JSON;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import javax.inject.Inject;
import javax.ws.rs.*;
import javax.ws.rs.core.Response;

@Path("/users")
public class UserRest {

  @Inject
  private UserService userService;

  public UserRest() {

  }

  /**
   * Route to get all the users
   *
   * @return  response containing a list of users in JSON format
   */
  @GET
  @Produces({ APPLICATION_JSON })
  public Response getUsers() {
    List<UserDto> users = new ArrayList<>();
    int status = 200;

    try {
      users = userService.getUsers().stream().map(UserDto::fromUser).collect(Collectors.toList());
    } catch (Exception e) {
      // TODO: explicit exception handling
      status = 500;
    }

    return Response
      .status(status)
      .entity(users)
      .build();
  }

  /**
   * Route to get a specific user with the username
   *
   * @param username  the username to find a user with
   * @return          response containing a user in JSON format
   */
  @GET
  @Path("/{username}")
  @Produces({ APPLICATION_JSON })
  public Response getUserByUsername(@PathParam("username") String username) {
    UserDto userDto = null;
    int status = 200;

    try {
      userDto = UserDto.fromUser(userService.getUserByUsername(username));
    } catch (Exception e) {
      // TODO: explicit exception handling
      status = 500;
    }

    return Response
      .status(status)
      .entity(userDto)
      .build();
  }

  /**
   * Route update an existing user's data
   *
   * Required request body JSON format:
   *  {
   *    "id": 1,
   *    "password": "updated",
   *    "username": "updated",
   *    "bio": "updated",
   *    "web": "updated",
   *    "imageUrls": "updated"
   *  }
   *
   * Only the id fields are required, all other field will get updated
   *
   * Note: field with the value of null get updated as well so make sure to include values who don't need to get updated
   *
   * @param user      user instance serialized from request body
   * @return          response containing user instance including in JSON format when successfully saved in the db
   */
  @PUT
  @Consumes({ APPLICATION_JSON })
  @Produces({ APPLICATION_JSON })
  public Response update(User user) {
    UserDto userDto = null;
    int status = 200;

    try {
      userDto = UserDto.fromUser(userService.update(user));
    } catch (Exception e) {
      // TODO: explicit exception handling
      status = 500;
    }

    return Response
      .status(status)
      .entity(userDto)
      .build();
  }

  /**
   * Route to delete existing user
   *
   * Will return successful response without content (204) on success
   *
   * @param id    the id of the user who needs to be deleted from the db
   */
  @DELETE
  @Path("/{id}")
  public void delete(@PathParam("id") Long id) {
    userService.delete(id);
  }

  /**
   * Route to add a new follow relation in between two users
   *
   * @param username        the username of the user who is gonna follow someone else
   * @param otherUsername   the username of the user who is going to be followed
   */
  @POST
  @Path("/follow/{username}/{otherUsername}")
  public void addFollower(@PathParam("username") String username, @PathParam("otherUsername") String otherUsername) {
    User user1 = userService.getUserByUsername(username);
    User user2 = userService.getUserByUsername(otherUsername);

    if (user1 == null || user2 == null) {
      return;
    }

    user1.follow(user2);
    userService.update(user1);
  }

  /**
   * Route to remove an existing follow relation in between two users
   *
   * @param username        the username of the user who is gonna un follow someone else
   * @param otherUsername   the username of the user who is going to be un followed
   */
  @DELETE
  @Path("/follow/{username}/{otherUsername}")
  public void deleteFollower(@PathParam("username") String username, @PathParam("otherUsername") String otherUsername) {
    User user1 = userService.getUserByUsername(username);
    User user2 = userService.getUserByUsername(otherUsername);

    if (user1 == null || user2 == null) {
      return;
    }

    user1.unFollow(user2);
    userService.update(user1);
  }
}
