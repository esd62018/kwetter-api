package rest;

import service.UserService;

import javax.inject.Inject;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.core.Response;

@Path("/mail")
public class MailRest {

  @Inject
  private UserService userService;

  public MailRest() {

  }

  /**
   * Route to confirm an account
   */
  @GET
  @Path("/{username}/{hash}")
  public Response confirm(@PathParam("username") String username, @PathParam("hash") String hash) {
    String message = null;
    int status;

    try {
      if (userService.setIsConfirmed(username, hash) == 1) {
        message = "Your account has been activated";
        status = 200;
      } else {
        message = "We can't find were you're looking for...";
        status = 404;
      }

    } catch (Exception e) {
      e.printStackTrace();

      message = "Oops, something went wrong!";
      status = 500;
    }

    return Response
      .status(status)
      .entity(message)
      .build();
  }
}
