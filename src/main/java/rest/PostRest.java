package rest;

import domain.Post;
import domain.Rating;
import domain.User;
import dto.PostDto;
import dto.RatingDto;
import service.PostService;
import service.UserService;

import javax.inject.Inject;
import javax.ws.rs.*;
import javax.ws.rs.core.Response;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import static javax.ws.rs.core.MediaType.APPLICATION_JSON;

@Path("/posts")
public class PostRest {

  @Inject
  private PostService postService;

  public PostRest() {

  }

  /**
   * Route to get all the posts from a user
   *
   * @param userId  the user id to lookup all the posts from
   * @return        response containing a list of post in JSON format
   */
  @GET
  @Path("/{userId}")
  @Produces({ APPLICATION_JSON })
  public Response getPostsFromUser(@PathParam("userId") Long userId) {
    List<PostDto> posts = new ArrayList<>();
    int status = 200;

    try {
      posts = postService
        .findPostsFromUser(userId)
        .stream()
        .map(PostDto::fromPost)
        .collect(Collectors.toList());
    } catch (Exception e) {
      // TODO: explicit exception handling
      status = 500;
    }

    return Response
      .status(status)
      .entity(posts)
      .build();
  }

  /**
   * Route to get all posts for one user's timeline
   *
   * @param userId    the id of the user to get all the posts for
   * @return          a response containing the posts in JSON format
   */
  @GET
  @Path("/timeline/{userId}")
  @Produces({ APPLICATION_JSON })
  public Response getTimeLinePostsFromUser(@PathParam("userId") Long userId) {
    List<PostDto> posts = new ArrayList<>();
    int status = 200;

    try {
      posts = postService
        .findTimeLinePostsFromUser(userId)
        .stream()
        .map(PostDto::fromPost)
        .collect(Collectors.toList());
    } catch (Exception e) {
      // TODO: explicit exception handling
      status = 500;
    }

    return Response
      .status(status)
      .entity(posts)
      .build();
  }

  /**
   * Route to search posts on post content
   *
   * @param content   the content to lookup all matching posts from
   * @return          response containing list of post in JSON format
   */
  @GET
  @Path("/search/{content}")
  @Produces({ APPLICATION_JSON })
  public Response getPostsWhereContentLike(@PathParam("content") String content) {
    List<PostDto> posts = new ArrayList<>();
    int status = 200;

    try {
      posts = postService
        .findPostsWhereContentLike(content)
        .stream()
        .map(PostDto::fromPost)
        .collect(Collectors.toList());
    } catch (Exception e) {
      // TODO: explicit exception handling
      status = 500;
    }

    return Response
      .status(status)
      .entity(posts)
      .build();
  }

  /**
   * Route to search posts where a user is mentioned in
   *
   * @param username  the username to lookup all the mentioned posts from
   * @return          response containing a list of post in JSON format
   */
  @GET
  @Path("/mentioned/{username}")
  @Produces({ APPLICATION_JSON })
  public Response getPostsWhereUserIsMentionedIn(@PathParam("username") String username) {
    List<PostDto> posts = new ArrayList<>();
    int status = 200;

    try {
      posts = postService
        .findPostsWhereUserIsMentionedIn(username)
        .stream()
        .map(PostDto::fromPost)
        .collect(Collectors.toList());
    } catch (Exception e) {
      // TODO: explicit exception handling
      status = 500;
    }

    return Response
      .status(status)
      .entity(posts)
      .build();
  }

  /**
   * Route to create a post
   *
   * Required request body JSON format:
   *  {
   *    "content": "content",
   *    "dateTime": "datetime string",
   *    "poster": {
   *        "id": posterId
   *    }
   *  }
   *
   * All above fields are required!
   *
   * @param post      post instance serialized from request body
   * @return          response containing post instance in JSON format when successfully saved in the db
   */
  @POST
  @Consumes({ APPLICATION_JSON })
  @Produces({ APPLICATION_JSON })
  public Response create(Post post) {
    PostDto postDto = null;
    int status = 200;

    try {
      postDto = PostDto.fromPost(postService.create(post));
    } catch (Exception e) {
      e.printStackTrace();
      // TODO: explicit exception handling
      status = 500;
    }

    return Response
      .status(status)
      .entity(postDto)
      .build();
  }

  /**
   * Route to update an existing post
   *
   * Required request body JSON format:
   *  {
   *    "id": id
   *    "content": "updated",
   *    "dateTime": "datetime string",
   *    "poster": {
   *        "id": posterId
   *    }
   *  }
   *
   * Only the id fields are required, all other field will get updated
   *
   * Note: field with the value of null get updated as well so make sure to include values who don't need to get updated
   *
   * @param post      post instance serialized from request body
   * @return          response containing post instance in JSON format when successfully saved in the db
   */
  @PUT
  @Consumes({ APPLICATION_JSON })
  @Produces({ APPLICATION_JSON })
  public Response update(Post post) {
    PostDto postDto = null;
    int status = 200;

    try {
      postDto = PostDto.fromPost(postService.update(post));
    } catch (Exception e) {
      // TODO: explicit exception handling
      status = 500;
    }

    return Response
      .status(status)
      .entity(postDto)
      .build();
  }

  /**
   * Route to delete an existing post
   *
   * Will return successful response without content (204) on success
   *
   * @param id    the id of the post which needs to be deleted from the db
   * @return      response containing status
   */
  @DELETE
  @Path("/{id}")
  public Response delete(@PathParam("id") Long id) {
    int status = 200;

    try {
      postService.delete(id);
    } catch (Exception e) {
      // TODO: explicit exception handling
      status = 500;
    }

    return Response
      .status(status)
      .build();
  }

  /**
   * Route to add a new rating to a post
   *
   * Required request body: {
   *     "userId": "id",
   *     "postId": "postId"
   * }
   *
   * All params are required
   * Id's need to exist as well
   *
   * @return        response containing a status 200 (OK) including newly saved rating in JSON format
   *                or success or 500 (Internal server error) on fail
   */
  @POST
  @Path("/ratings")
  @Consumes({ APPLICATION_JSON })
  @Produces({ APPLICATION_JSON })
  public Response addRating(RatingDto ratingDto) {
    Long postId = ratingDto.getPostId();
    Rating rating = new Rating();
    rating.setRater(new User(ratingDto.getUserId()));
    rating.setRatedPost(new Post(ratingDto.getPostId()));
    int status = 200;

    try {
      ratingDto = RatingDto.fromRating(postService.addRating(postId, rating));
    } catch (Exception e) {
      // TODO: explicit exception handling
      status = 500;
      ratingDto = null;
    }

    return Response
      .status(status)
      .entity(ratingDto)
      .build();
  }

  /**
   * Route to delete a rating from a post
   *
   * @return              response containing a status 204 (No Content)
   *                      or success or 500 (Internal server error) on fail
   */
  @DELETE
  @Path("/ratings/{postId}/{ratingId}")
  public Response deleteRating(@PathParam("postId") Long postId, @PathParam("ratingId") Long ratingId) {
    int status = 200;

    try {
      postService.deleteRating(postId, ratingId);
    } catch (Exception e) {
      // TODO: explicit exception handling
      status = 500;
    }

    return Response
      .status(status)
      .build();
  }
}
