package dao;

import domain.Post;
import domain.Rating;
import domain.User;

import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

@Stateless
public class PostDao {

  @PersistenceContext
  private EntityManager entityManager;

  public PostDao() {

  }

  public List<Post> findAllPostsOrderedByDate() {
    return entityManager
      .createNamedQuery("Post.findAllOrderedByDate", Post.class)
      .getResultList();
  }

  public List<Post> findPostsFromUserId(Long id) {
    return entityManager
      .createNamedQuery("Post.findAllFromUser", Post.class)
      .setParameter("userId", id)
      .getResultList();
  }

  public List<Post> findPostsWhereContentLike(String content) {
    return entityManager
      .createNamedQuery("Post.findAllWhereContentLike", Post.class)
      .setParameter("content", "%" + content + "%")
      .getResultList();
  }


  public Post create(Post post) {
    User poster = entityManager.find(User.class, post.getPoster().getId());
    post.setPoster(poster);

    entityManager.persist(post);

    return post;
  }

  public Post update(Post post) {
    entityManager.merge(post);

    return post;
  }

  public void delete(Long id) {
    entityManager.remove(entityManager.find(Post.class, id));
  }

  public Rating addRating(Long postId, Rating rating) {
    entityManager.merge(rating);
    Post post = entityManager.find(Post.class, postId);
    post.addRating(rating);
    //entityManager.merge(post);

    return rating;
  }

  public void deleteRating(Long postId, Long ratingId) {
    Post post = entityManager.find(Post.class, postId);
    post.deleteRating(ratingId);
    entityManager.merge(post);
  }
}
