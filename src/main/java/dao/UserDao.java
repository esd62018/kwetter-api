package dao;

import domain.User;
import domain.UserRole;

import java.io.Serializable;
import java.util.List;

import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

@Stateless
public class UserDao implements Serializable {

  @PersistenceContext
  private EntityManager entityManager;

  public UserDao() {

  }

  public List<User> findAllUsers() {
    return entityManager
      .createNamedQuery("User.findAll", User.class)
      .getResultList();
  }

  public User findUserByLoginCredential(String username, String password) {
    return entityManager
      .createNamedQuery("User.auth", User.class)
      .setParameter("username", username)
      .setParameter("password", password)
      .getSingleResult();
  }

  public User findUserById(Long id) {
    return entityManager.find(User.class, id);
  }

  public User findUserByUsername(String username) {
    return entityManager
      .createNamedQuery("User.findByUsername", User.class)
      .setParameter("username", username)
      .getSingleResult();
  }

  public User create(User user) {
    entityManager.persist(user);

    return user;
  }

  public User update(User user) {
    entityManager.merge(user);

    return user;
  }

  public void delete(Long id) {
    entityManager.remove(entityManager.find(User.class, id));
  }

  public void switchUserRole(Long id) {
    User user = entityManager.find(User.class, id);
    UserRole userRole = user.getRole();
    userRole.setRole(userRole.getRole().equals("user") ? "admin" : "user");
    update(user);
  }

  public int setIsConfirmed(String username, String hash) {
    return entityManager.createNamedQuery("User.setIsConfirmed", User.class)
      .setParameter("username", username)
      .setParameter("password", hash)
      .executeUpdate();
  }
}
