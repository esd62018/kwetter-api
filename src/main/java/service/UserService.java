package service;

import dao.UserDao;
import domain.User;
import utils.Constants;
import utils.Hash;
import utils.Mailer;

import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.mail.MessagingException;
import java.io.Serializable;
import java.security.NoSuchAlgorithmException;
import java.util.List;

@Stateless
public class UserService implements Serializable {

  @Inject
  private UserDao userDao;

  public UserService() {

  }

  /**
   * Gets all existing users from the database
   *
   * @return    a list of all users from the database
   */
  public List<User> getUsers() {
    return userDao.findAllUsers();
  }

  /**
   * Will 'authenticate' a user.
   *
   * It looks up a user from the database where the username and password matches a record in the database.
   *
   * @param user    the user to check the login credentials from in the database
   * @return        a User when found otherwise null
   */
  public User authenticate(User user) throws NoSuchAlgorithmException {
    String hashedPassword = Hash.hashPassword(user.getPassword());

    return userDao.findUserByLoginCredential(user.getUsername(), hashedPassword);
  }

  /**
   * Returns the user queried by username
   *
   * @param username  the user to query from the database
   * @return          a user when found in the database
   */
  public User getUserByUsername(String username) {
    return userDao.findUserByUsername(username);
  }

  /**
   * Will save a new user instance in the database.
   *
   * @param user    a new user instance
   * @return        the newly saved user instance
   */
  public User signUp(User user) throws NoSuchAlgorithmException, MessagingException {
    String hashedPassword = Hash.hashPassword(user.getPassword());

    Mailer.sendConfirmationEmail(user.getUsername(), hashedPassword);

    user.setPassword(hashedPassword);

    return userDao.create(user);
  }

  /**
   * Will update an existing user record from the database.
   *
   * @param user    an existing user record but then with changed values
   * @return        the updated user instance
   */
  public User update(User user) {
    return userDao.update(user);
  }

  /**
   * Will delete an existing user from the database.
   *
   * @param id    the id of a user to delete
   */
  public void delete(Long id) {
    userDao.delete(id);
  }

  /**
   * Will update a role of an existing user
   *
   * There are two possible user roles, so this will mean the following:
   * If the current user role is USER it will be updated to ADMIN and visa versa
   *
   * @param id      the id of the user to switch the role for
   */
  public void switchUserRole(Long id) {
    userDao.switchUserRole(id);
  }

  /**
   * Will set is confirmed to 1 to an existing user
   *
   * @param username    the name of the user
   * @param hash        the hash of the password to search the user from
   * @return            number of updated records (should be 1)
   */
  public int setIsConfirmed(String username, String hash) {
    return userDao.setIsConfirmed(username, hash);
  }
}
