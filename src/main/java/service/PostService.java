package service;

import dao.PostDao;
import dao.UserDao;
import domain.Post;
import domain.Rating;
import domain.User;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;

import javax.ejb.Stateless;
import javax.inject.Inject;

@Stateless
public class PostService {

  @Inject
  private PostDao postDao;

  @Inject
  private UserDao userDao;

  public PostService() {

  }

  /**
   * Gets all posts ordered by date DESC
   *
   * @return      a list of posts from the user
   */
  public List<Post> findAllPostsOrderedByDate() {
    return postDao.findAllPostsOrderedByDate();
  }

  /**
   * Gets all posts from a user where the user id matches a post's POSTER_ID
   *
   * @param id    the id of the user to find the posts for
   * @return      a list of posts from the user
   */
  public List<Post> findPostsFromUser(Long id) {
    return postDao.findPostsFromUserId(id);
  }

  /**
   * Gets all posts a user should see on its timeline.
   *
   * This included all its own posts + all its following users posts.
   *
   * @param id      the id of the user to get the posts for
   * @return        a lists of posts
   */
  public List<Post> findTimeLinePostsFromUser(Long id) {
    User user = userDao.findUserById(id);
    List<Post> posts = findPostsFromUser(id);

    for (User follower: user.getFollows()) {
      posts.addAll(findPostsFromUser(follower.getId()));
    }

    return posts;
  }

  /**
   * Gets all posts where the content is %like%. Can be used as search functionality.
   *
   * @param content   the content to search for
   * @return          a list of posts where the content matches the search
   */
  public List<Post> findPostsWhereContentLike(String content) {
    return postDao.findPostsWhereContentLike(content);
  }

  /**
   * Gets all posts where the content contains a username.
   * Can be used as search functionality to find posts where a user is 'mentioned' in.
   *
   * What does mentioned mean?
   * It means that a post contains content where the username occurs in as a 'tag'.
   * This tag will look like: @username.
   *
   * @param userName    the username to find mentioned posts for
   * @return            a list of posts where the user is mentioned in
   */
  public List<Post> findPostsWhereUserIsMentionedIn(String userName) {
    String tag = "@" + userName;

    return postDao.findPostsWhereContentLike(tag);
  }

  /**
   * Will save a new post instance in the database.
   *
   * @param post    a new post instance to save in the database
   * @return        the newly create post instance
   */
  public Post create(Post post) {
    return postDao.create(post);
  }

  /**
   * Will update an existing post record in the database.
   *
   * @param post    an existing post record but then with changed values
   * @return        the updated post instance
   */
  public Post update(Post post) {
    return postDao.update(post);
  }

  /**
   * Will delete an existing post record from the database.
   *
   * @param id    the id of the post to delete
   */
  public void delete(Long id) {
    postDao.delete(id);
  }

  /**
   * Will add a new rating to an existing post
   *
   * @param postId    the id for the post to add the rating to
   * @param rating    a new rating instance to save in the database
   * @return          the newly create rating instance
   */
  public Rating addRating(Long postId, Rating rating) {
    return postDao.addRating(postId, rating);
  }

  /**
   * Will delete an existing rating from a post
   *
   * @param postId      the id of the post to delete the rating from
   * @param ratingId    the id of the rating to delete
   */
  public void deleteRating(Long postId, Long ratingId) {
    postDao.deleteRating(postId, ratingId);
  }
}
