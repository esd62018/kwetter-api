ALTER TABLE User_Role DROP FOREIGN KEY `FK_User_Role_USERNAME`;

ALTER TABLE User_Role ADD CONSTRAINT `FK_User_Role_USERNAME` FOREIGN KEY (`USERNAME` ) REFERENCES `User` (`USERNAME` ) ON DELETE CASCADE;

ALTER TABLE Post DROP FOREIGN KEY `FK_Post_POSTER_ID`;

ALTER TABLE Post ADD CONSTRAINT `FK_Post_POSTER_ID` FOREIGN KEY (`POSTER_ID` ) REFERENCES `User` (`ID` ) ON DELETE CASCADE;

ALTER TABLE Rating DROP FOREIGN KEY `FK_Rating_POST_ID`;

ALTER TABLE Rating ADD CONSTRAINT `FK_Rating_POST_ID` FOREIGN KEY (`POST_ID` ) REFERENCES `Post` (`ID` ) ON DELETE CASCADE;

INSERT INTO User (ID, BIO, IMAGEURL, PASSWORD, USERNAME, WEB, IS_CONFIRMED) VALUES (1, 'Ik zit op Fontys Hogescholen ICT', 'https://img.etsystatic.com/il/ef6030/421508483/il_570xN.421508483_b0f0.jpg', 'XohImNooBHFR0OVvjcYpJ3NgPQ1qq73WKhHvch0VQtg=', 'Jim', 'https://www.facebook.com/jim', true);
INSERT INTO User (ID, BIO, IMAGEURL, PASSWORD, USERNAME, WEB, IS_CONFIRMED) VALUES (2, 'Mijn favoriete groente is kip', 'http://emanuelstreickernyc.org/wp-content/uploads/2017/08/barack-obama.jpg', 'XohImNooBHFR0OVvjcYpJ3NgPQ1qq73WKhHvch0VQtg=', 'Obama', 'https://www.facebook.com/obama', true);
INSERT INTO User (ID, BIO, IMAGEURL, PASSWORD, USERNAME, WEB, IS_CONFIRMED) VALUES (3, 'Donald J. Trump, heerser van Amerika maar eigenlijk de hele wereld', 'https://pixel.nymag.com/imgs/daily/intelligencer/2018/02/23/23-donald-trump.w710.h473.jpg', 'XohImNooBHFR0OVvjcYpJ3NgPQ1qq73WKhHvch0VQtg=', 'Trump', 'https://www.facebook.com/trump', true);
INSERT INTO User (ID, BIO, IMAGEURL, PASSWORD, USERNAME, WEB, IS_CONFIRMED) VALUES (4, 'Ik houd van katten', 'https://www.biography.com/.image/ar_1:1%2Cc_fill%2Ccs_srgb%2Cg_face%2Cq_80%2Cw_300/MTE4MDAzNDEwMDU4NTc3NDIy/hillary-clinton-9251306-2-402.jpg', 'XohImNooBHFR0OVvjcYpJ3NgPQ1qq73WKhHvch0VQtg=', 'Clinton', 'https://www.facebook.com/clinton', true);
INSERT INTO User (ID, BIO, IMAGEURL, PASSWORD, USERNAME, WEB, IS_CONFIRMED) VALUES (5, 'Ik val net zo hard voor jou als dat mijn kabinet valt ;)', 'https://upload.wikimedia.org/wikipedia/commons/thumb/d/d3/Mark_Rutte_2015_%281%29.jpg/266px-Mark_Rutte_2015_%281%29.jpg', 'XohImNooBHFR0OVvjcYpJ3NgPQ1qq73WKhHvch0VQtg=', 'Rutte', 'https://www.facebook.com/rutte', true);
INSERT INTO User (ID, BIO, IMAGEURL, PASSWORD, USERNAME, WEB, IS_CONFIRMED) VALUES (7, 'Ik worstel met beren', 'https://timedotcom.files.wordpress.com/2018/03/gettyimages-932825264.jpg', 'XohImNooBHFR0OVvjcYpJ3NgPQ1qq73WKhHvch0VQtg=', 'Putin', 'https://www.facebook.com/putin', true);
INSERT INTO User (ID, BIO, IMAGEURL, PASSWORD, USERNAME, WEB, IS_CONFIRMED) VALUES (100, NULL, NULL, '5e884898da28047151d0e56f8dc6292773603d0d6aabbdd62a11ef721d1542d8', 'admin', NULL, true);

INSERT INTO Post (ID, POSTER_ID, CONTENT, DATETIME) VALUES (1, 1, 'Twitter is dead and Kwetter killed it' , '2018-03-15 18:14:23');
INSERT INTO Post (ID, POSTER_ID, CONTENT, DATETIME) VALUES (2, 1, 'Vandaag lekker aan Kwetter werken.' , '2018-03-15 17:55:12');
INSERT INTO Post (ID, POSTER_ID, CONTENT, DATETIME) VALUES (3, 3, 'Is Noord-Korea al wakker? Ik vraag het voor een vriend.' , '2018-03-13 17:48:23');
INSERT INTO Post (ID, POSTER_ID, CONTENT, DATETIME) VALUES (4, 4, 'Vandaag weer niks voor elkaar gekregen en alleen maar kattenvideos gekeken op YouTube', '2018-03-14 21:58:23');
INSERT INTO Post (ID, POSTER_ID, CONTENT, DATETIME) VALUES (5, 5, 'De Luizenmoeder is echt mijn guilty pleasure serie. Geweldige kwaliteit van Nederlandse bodem!' , '2018-03-14 18:06:27');
INSERT INTO Post (ID, POSTER_ID, CONTENT, DATETIME) VALUES (6, 5, 'Willen wij meer of minder Luizenmoeder? Meer meer meer!' , '2018-03-14 23:27:04');
INSERT INTO Post (ID, POSTER_ID, CONTENT, DATETIME) VALUES (9, 7, 'Lmao, @Trump is echt koning' , '2018-03-13 22:25:46');
INSERT INTO Post (ID, POSTER_ID, CONTENT, DATETIME) VALUES (10, 7, 'Wie wil er een lekker kopje thee met mij komen drinken?' , '2018-03-12 18:34:23');
INSERT INTO Post (ID, POSTER_ID, CONTENT, DATETIME) VALUES (11, 3, 'Covfefe' , '2018-03-14 20:03:59');
INSERT INTO Post (ID, POSTER_ID, CONTENT, DATETIME) VALUES (12, 4, 'Hahaha, deze kattenvideo! https://www.youtube.com/watch?v=jX3iLfcMDCw' , '2018-03-13 18:06:12');
INSERT INTO Post (ID, POSTER_ID, CONTENT, DATETIME) VALUES (13, 1, 'JEA volgen' , '2018-03-12 11:00:14');
INSERT INTO Post (ID, POSTER_ID, CONTENT, DATETIME) VALUES (14, 3, 'Vandaag chillen met mijn bestie @Putin' , '2018-03-11 23:52:38');
INSERT INTO Post (ID, POSTER_ID, CONTENT, DATETIME) VALUES (15, 7, '@Obama cyka blyat' , '2018-03-10 22:25:13');
INSERT INTO Post (ID, POSTER_ID, CONTENT, DATETIME) VALUES (16, 7, '@Jim Lekker gewerkt' , '2018-03-10 22:25:13');

INSERT INTO Rating (ID, POST_ID, USER_ID) VALUES (1, 1, 1);
INSERT INTO Rating (ID, POST_ID, USER_ID) VALUES (2, 1, 2);
INSERT INTO Rating (ID, POST_ID, USER_ID) VALUES (3, 1, 3);
INSERT INTO Rating (ID, POST_ID, USER_ID) VALUES (4, 1, 4);

INSERT INTO User_User(follows_ID, FollowedBys_ID) VALUES(1, 2);
INSERT INTO User_User(follows_ID, FollowedBys_ID) VALUES(1, 3);
INSERT INTO User_User(follows_ID, FollowedBys_ID) VALUES(1, 4);
INSERT INTO User_User(follows_ID, FollowedBys_ID) VALUES(1, 5);
INSERT INTO User_User(follows_ID, FollowedBys_ID) VALUES(2, 1);
INSERT INTO User_User(follows_ID, FollowedBys_ID) VALUES(2, 3);
INSERT INTO User_User(follows_ID, FollowedBys_ID) VALUES(2, 4);
INSERT INTO User_User(follows_ID, FollowedBys_ID) VALUES(2, 5);
INSERT INTO User_User(follows_ID, FollowedBys_ID) VALUES(3, 1);
INSERT INTO User_User(follows_ID, FollowedBys_ID) VALUES(3, 2);
INSERT INTO User_User(follows_ID, FollowedBys_ID) VALUES(3, 4);
INSERT INTO User_User(follows_ID, FollowedBys_ID) VALUES(3, 5);
INSERT INTO User_User(follows_ID, FollowedBys_ID) VALUES(3, 7);
INSERT INTO User_User(follows_ID, FollowedBys_ID) VALUES(4, 1);
INSERT INTO User_User(follows_ID, FollowedBys_ID) VALUES(4, 2);
INSERT INTO User_User(follows_ID, FollowedBys_ID) VALUES(4, 3);
INSERT INTO User_User(follows_ID, FollowedBys_ID) VALUES(4, 5);
INSERT INTO User_User(follows_ID, FollowedBys_ID) VALUES(4, 7);
INSERT INTO User_User(follows_ID, FollowedBys_ID) VALUES(5, 1);

INSERT INTO User_Role(ID, ROLE, USERNAME) VALUES (1, 'user', 'Jim');
INSERT INTO User_Role(ID, ROLE, USERNAME) VALUES (2, 'user', 'Obama');
INSERT INTO User_Role(ID, ROLE, USERNAME) VALUES (3, 'user', 'Trump');
INSERT INTO User_Role(ID, ROLE, USERNAME) VALUES (4, 'user', 'Clinton');
INSERT INTO User_Role(ID, ROLE, USERNAME) VALUES (5, 'user', 'Rutte');
INSERT INTO User_Role(ID, ROLE, USERNAME) VALUES (7, 'user', 'Putin');
INSERT INTO User_Role(ID, ROLE, USERNAME) VALUES (8, 'admin', 'admin');
